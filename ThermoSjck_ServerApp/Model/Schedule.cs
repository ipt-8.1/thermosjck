﻿namespace ThermoSjck_ServerApp.Model
{
    public class Schedule
    {
        public List<ScheduleEntry> Entries { get; set; }
    }

    public class ScheduleEntry
    {
        public string DayOfWeek { get; set; }
        public string Hour { get; set; }
        public string Minute { get; set; }
        public string Temperature { get; set; }
    }
}