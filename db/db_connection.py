from pymongo import MongoClient
from bson import ObjectId

class db_connection:
    def __init__(self):
        #file = open('/usr/app/src/Volume/mongo_connection_string.txt', "r")
        #connection_string_path = file.read()
        self.client = MongoClient("mongodb://root:example@localhost:27017/")
    
    def get_Databases(self):
        dblist = self.client.list_database_names()
        return dblist
        
    def check_for_Database(self, name):
        dblist = self.client.list_database_names()
        return name in dblist
        
    def select_Database(self, name):
        self.db = self.client[name]
        self.dbname = name
        
    def get_Db_name(self):
        return self.dbname
        
    def get_Collections(self):
        collist = self.db.list_collection_names()
        return collist
        
    def select_Collection(self, name):
        self.collection = self.db[name]
        self.collectionname = name
        
    def get_Collection_name(self):
        return self.collectionname
            
    def get_Documents(self):
        documentlist = []
        for document in self.collection.find():
            documentlist.append(document)
        return documentlist
    
    def check_for_Document(self, name, object):
        documentlist = []
        for document in self.collection.find():
            documentlist.append(document[object])
        return name in documentlist
            
    def select_Document(self,name):
        query = {"_id": ObjectId(name)}
        self.document = self.collection.find_one(query)
        self.documentname = name
        
    def get_Document_name(self):
        return self.documentname
        
    def get_Content(self):
        return self.document
        
    def add_Document(self,content):
        self.collection.insert_one(content.__dict__)
