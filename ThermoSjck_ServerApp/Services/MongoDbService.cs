﻿using MongoDB.Driver;
using System;
using ThermoSjck_ServerApp.Model;

public class MongoDbService
{
    private readonly IMongoCollection<Schedule> _scheduleCollection;

    public MongoDbService(IConfiguration configuration)
    {
        var client = new MongoClient(configuration.GetConnectionString("MongoDB"));
        var database = client.GetDatabase("ThermoSjck");
        _scheduleCollection = database.GetCollection<Schedule>("Schedules");
    }

    public List<Schedule> GetSchedules()
    {
        return _scheduleCollection.Find(schedule => true).ToList();
    }

    public void AddPerson(Schedule schedule)
    {
        _scheduleCollection.InsertOne(schedule);
    }
}
