from datetime import datetime

class db_entry:
    def __init__(self, RoomTemperature: float, FlowTemperature: float, WaterTankTemperature: float, PumpState: bool, ValveState: int):
        self.timestamp=datetime.now()
        self.RoomTemerature=RoomTemperature
        self.FlowTemperature=FlowTemperature
        self.WaterTankTemperature=WaterTankTemperature
        self.PumpState=PumpState
        self.ValveState=ValveState
