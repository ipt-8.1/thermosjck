﻿using System;
using System.Collections.Generic;
using MongoDB.Driver;
using ThermoSjck_ServerApp.Model;

namespace ThermoSjck_ServerApp.Services
{
    public class ScheduleService
    {
        private readonly IMongoCollection<Schedule> _scheduleCollection;

        public ScheduleService(IMongoDatabase database)
        {
            _scheduleCollection = database.GetCollection<Schedule>("schedule");
        }
        public List<ScheduleEntry> GetScheduleEntries()
        {
            // Implement logic to retrieve schedule entries from the database
            return _scheduleCollection.Find(_ => true).FirstOrDefault()?.Entries ?? new List<ScheduleEntry>();
        }

        public void SaveScheduleEntry(ScheduleEntry entry)
        {
            var schedule = new Schedule
            {
                Entries = GetScheduleEntries()
            };

            schedule.Entries.Add(entry);

            _scheduleCollection.ReplaceOne(_ => true, schedule, new ReplaceOptions { IsUpsert = true });
        }
    }
}
