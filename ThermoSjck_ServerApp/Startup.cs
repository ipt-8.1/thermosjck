﻿//using Microsoft.AspNetCore.Builder;
//using Microsoft.AspNetCore.Hosting;
//using Microsoft.Extensions.Configuration;
//using Microsoft.Extensions.DependencyInjection;
//using Microsoft.Extensions.Hosting;
//using ThermoSjck_ServerApp.Model; 

//namespace ThermoSjck_ServerApp
//{
//    public class Startup
//    {
//        public Startup(IConfiguration configuration)
//        {
//            Configuration = configuration;
//        }

//        public IConfiguration Configuration { get; }

//        public void ConfigureServices(IServiceCollection services)
//        {
//            // Fügen Sie hier Ihre MongoDB-Konfiguration hinzu
//            services.AddSingleton<IMongoDbService, MongoDbService>();
//        }

//        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
//        {
//            if (env.IsDevelopment())
//            {
//                app.UseDeveloperExceptionPage();
//                app.UseWebAssemblyDebugging();
//            }
//            else
//            {
//                app.UseExceptionHandler("/Home/Error");
//                app.UseHsts();
//            }

//            app.UseHttpsRedirection();
//            app.UseBlazorFrameworkFiles();
//            app.UseStaticFiles();

//            app.UseRouting();

//            app.UseEndpoints(endpoints =>
//            {
//                endpoints.MapRazorPages();
//                endpoints.MapControllers();
//                endpoints.MapFallbackToFile("index.html");
//            });
//        }
//    }
//}
