<?php
 


$dataPoints = array();
$y = 40;
for($i = 0; $i < 10; $i++){
	$y += rand(0, 10) - 5; 
	array_push($dataPoints, array("x" => $i, "y" => $y));
}
 
?>


<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">    <title>Document</title>
        <link href="style.css" rel="stylesheet">
    </head>
    <body class="mx-auto">
        <header class="fixed-top mx-auto">
            <div class="d-flex mb-3 p-4">
                <div class="me-auto">
                    <a href="index.html"><h1>ThermoSjck</h1></a>
                </div>
                <div class="p-2 align-self-center">
                    <a href="#">Home</a>
                </div>
                <div class="p-2 align-self-center">
                    <a href="#">Overview</a>
                </div>
                <div class="p-2 align-self-center">
                    <a href="#">Timeline</a>
                </div>
                <div class="p-2 align-self-center">
                    <a href="#">Schedule</a>
                </div>
            </div>
        </header>
        <main class="p-4">
            <section id="home">
                <h1 class="text-center">Home</h1>
                <hr>
                <div class="input-group mb-3">
                    <label class="input-group-text" for="inputGroupSelect01">Zeitspanne</label>
                    <select class="form-select" id="inputGroupSelect01">
                      <option selected>Wählen...</option>
                      <option value="1">1 Tag</option>
                      <option value="30">1 Monat</option>
                      <option value="365">1 Jahr</option>
                    </select>
                  </div>
                <div class="container text-center">
                    <div class="row align-items-center">
                        <div id="chartContainer" class="col-8 mx-5" style="height:60vh;"></div>
                        <div class="col">
                            <div>
                                <h3>Durchschnitt</h3>
                                <h1>29.3°C</h1>
                            </div>
                            <hr>
                            <div>
                                <h3>Mind. Temperatur</h3>
                                <h1>28.1°C</h1>
                            </div>
                            <hr>
                            <div>
                                <h3>Max. Temperatur</h3>
                                <h1>32.5°C</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
    </body>
    <script>
        window.onload = function () {
        
        var chart = new CanvasJS.Chart("chartContainer", {
            theme: "light1", // "light1", "light2", "dark1", "dark2"
            animationEnabled: true,
            zoomEnabled: true,
            backgroundColor: "#f4f4f4",
            title: {
                text: "Temperature Timeline"
            },
            data: [{
                type: "area",     
                dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
            }]
        });
        chart.render();
        
        }
    </script>
    <script src="https://cdn.canvasjs.com/canvasjs.min.js"></script>
</html>